import pandas as pd
import geojson
import pyproj
import os
import numpy as np
import sys

from functools import partial
from shapely.geometry import  Point
from shapely.ops import transform
from settings import OUTPUT_FOLDER, logging

POINT_FOLDER ='geojson_points'

def feature_generator(df, insert_feutures, features):
    df.apply(insert_feutures, axis=1)
    feature = geojson.FeatureCollection(features)
    feature['type'] = "FeatureCollection"
    feature['crs'] = {}
    feature['crs']['type']='name'
    feature['crs']['properties']={}
    feature['crs']['properties']['name']="urn:ogc:def:crs:EPSG::32611"
    return feature

def data2geojson(df):
    features = []
    insert_features = lambda X: features.append(
            geojson.Feature(geometry=geojson.Point((X["mapX"],
                                                    X["mapY"])),
                            properties=dict(id=None,
                                            )))
    ref_feature = feature_generator(df, insert_features, features)

    base_pts_path = os.path.join(OUTPUT_FOLDER,POINT_FOLDER, 'base_pts.geojson')
    with open(base_pts_path, 'w', encoding='utf8') as fp:
        geojson.dump(ref_feature, fp, sort_keys=True, ensure_ascii=False)
    logging.info(base_pts_path)

    non_align_insert_features = lambda X: features.append(
            geojson.Feature(geometry=geojson.Point((X["pixelX"],
                                                    X["pixelY"])),
                            properties=dict(id=None,
                                            )))
    non_align_feature = feature_generator(df, non_align_insert_features, features)
    non_align_pts_path = os.path.join(OUTPUT_FOLDER,POINT_FOLDER, 'non_align_pts.geojson')

    with open(non_align_pts_path, 'w', encoding='utf8') as fp:
        geojson.dump(non_align_feature, fp, sort_keys=True, ensure_ascii=False)
    logging.info(non_align_pts_path)


def check_point_crs(pts, crs):
    if np.max(pts[0]) < 180 and np.min(pts[0]) > -180:
        project = partial(
            pyproj.transform,
            pyproj.Proj(init='epsg:4326'),  # source coordinate system
            pyproj.Proj(init=crs))  # destination coordinate system

        pts = [transform(project, Point(pt)) for pt in pts]
        pts = [np.array(pt) for pt in pts]
    return pts

def file_to_df(manual_pts):
    pts_df = pd.read_csv(manual_pts)
    return pts_df

if __name__ == '__main__':
    try:
        pts_df= file_to_df(sys.argv[1])
    except IndexError as err:
        logging.error("there is no path to point file in params")
        exit(-1)
    os.makedirs(OUTPUT_FOLDER, exist_ok=True)
    os.makedirs(os.path.join(OUTPUT_FOLDER,POINT_FOLDER), exist_ok=True)
    data2geojson(pts_df)