# preview_support_manual_alignment

## Quick start
#### Build docker image
    docker build . -t seetree/psma
#### Run container
    docker run -d seetree/psma
    
## Transformation Request  
#### - input:
    manual_points_path - path to file with manual points locally or in backet for calculating transformation matrix
    raster_path - path to raster, which requires alignment 
    output_path: - path for output data
#### - endpoint:
    /transform
#### - example curl request:
```
curl -H "Content-Type: application/json" 
    -H "Host: manual-alignment-adjuster.default.example.com" 
    -X POST http://35.193.139.249/transform 
    -d '{
            "raster_path":"gs://com_out/for_heorhii/for_alex/raster_composer/konda2/RGB_.tif", 
            "manual_points_path": "gs://com_out/aligment_test_data/konda-2_1-12/segmentation/14_29_agrowing_119/1-12/manual_gcp.points", 
            "output_path":"gs://com_out/for_heorhii/for_alex/raster_composer/konda2/"
        }'
```