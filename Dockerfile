FROM gcr.io/google-appengine/python
RUN apt-get update

RUN apt-get update
RUN apt-get install -y libproj-dev gdal-bin libgdal-dev python-gdal python3-gdal libsm6 libxext6 libfontconfig1 libxrender1 blt-dev

RUN apt-get install -y curl lsb-release
RUN echo "deb http://packages.cloud.google.com/apt cloud-sdk-jessie main" |  tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg |  apt-key add -
RUN apt-get update
RUN apt-get install -y google-cloud-sdk
RUN apt-get install -y gcc python-dev python-setuptools
RUN echo y | python2.7 -m pip uninstall crcmod
RUN python2.7 -m pip install -U crcmod

RUN apt-get install -y  python-numpy-dev
RUN apt-get install -y wkhtmltopdf

# Create a virtualenv for dependencies. This isolates these packages from
# system-level packages.
RUN virtualenv /env -p python3.7

# Setting these environment variables are the same as running
# source /env/bin/activate.
ENV VIRTUAL_ENV /env
ENV APP_HOME /app
ENV TEMP_FOLDER /data
ENV PATH /env/bin:$PATH
ENV PATH /usr/include/gdal:$PATH
ENV PYTHONPATH "${PYTHONPATH}:/app"
# Copy the application's requirements.txt and run pip to install all
# dependencies into the virtualenv.
ADD requirements.txt /app/requirements.txt
ADD requirements-private.txt /app/requirements-private.txt
RUN pip install -r /app/requirements.txt
RUN pip install -i https://seetree:suntreat2017@seetree-pip.appspot.com/pypi -r /app/requirements-private.txt


ADD . /data
ADD . /tmp
# Add the application source code.
ADD . /app
WORKDIR $APP_HOME
COPY . ./
EXPOSE 8080
# Run the web service on container startup. Here we use the gunicorn
# webserver, with one worker process and 8 threads.
# For environments with multiple CPU cores, increase the number of workers
# to be equal to the cores available.
CMD exec gunicorn --bind :8080 --workers 1 --threads 8 app:app
