import os
import json
import numpy as np
import cv2
import pandas as pd
import pyproj
import rasterio

from rasterio.windows import Window
from rasterio.plot import reshape_as_raster, reshape_as_image
from shapely.geometry import Point, Polygon
from functools import partial
from settings import logging, TILE_SIZE
from affine import Affine
from skimage.transform import ProjectiveTransform
from shapely.ops import transform

logger = logging.getLogger(__name__)


# def tiles_with_overlap(img, window_size, overlap):
#     sp = []
#     matrices = []
#     pnt_step = int(window_size * overlap)
#     step_h = img.shape[1]//pnt_step
#     step_w = img.shape[0]//pnt_step
#     pointerh_min = 0
#     for h in range(step_h + 1):
#         if h != 0:
#             pointerh_min += pnt_step
#         pointerh = min(pointerh_min, img.shape[1])
#         pointerh_max = pointerh_min + window_size
#         pointerh_max = min(pointerh_min + window_size, img.shape[1])
#         pointerw_min = 0
#         if pointerh == pointerh_max:
#             continue
#         for w in range(step_w + 1):
#             if w != 0:
#                 pointerw_min += pnt_step
#             pointerw = min(pointerw_min, img.shape[0])
#             pointerw_max = pointerw_min + window_size
#             pointerw_max = min(pointerw_min + window_size, img.shape[0])
#             if pointerw == pointerw_max:
#                 #print("hi")
#                 continue
#             else:
#                 sp.append([pointerh, pointerh_max, pointerw, pointerw_max])
#                 matrices.append(img[pointerw:pointerw_max, pointerh:pointerh_max])
#     return matrices, sp

#
# def save_raster_path(save_path, meta, raster_array, downscale=0.1):
#     """
#     Save raster having array metadata and save path
#     :param save_path: path to save raster
#     :param meta: metadata
#     :param raster_array: raster_array
#     :return:
#     """
#     raster_array = cv2.resize(raster_array, None, fx=downscale, fy=downscale)
#     new_meta = meta.copy()
#     new_trans = (new_meta['transform'][0]/downscale, new_meta['transform'][1], new_meta['transform'][2],
#                  new_meta['transform'][3], new_meta['transform'][4]/downscale, new_meta['transform'][5])
#     new_meta.update({
#         "height": raster_array.shape[0],
#         "width": raster_array.shape[1],
#         "transform": new_trans,
#         "dtype": 'uint8',
#         "nodata": 0
#     })
#     if len(raster_array.shape) < 3:
#         raster_array = np.expand_dims(raster_array, axis=0)
#
#     with rasterio.open(save_path, 'w', **new_meta) as dst:
#         for i in range(1, new_meta['count'] + 1):
#             src_array = raster_array[:, :, i - 1]
#             dst.write(src_array, i)


def raster_to_img(raster, meta):
    cv_res = np.zeros((raster.shape[1], raster.shape[2], 3), dtype=np.uint8)
    count = meta['count']
    for i in range(count):
        band = raster[i]
        if meta['dtype'] in ['float32', 'float16']:
            band[raster[i]<0] = 0
            band = raster[i]*255
        cv_res[:, :, i] = band.astype(np.uint8)# / 5.87
    # cv_res[:, :, 1] = raster[1].astype(np.uint8)# / 5.95
    # cv_res[:, :, 2] = raster[2].astype(np.uint8)# / 5.95
    return cv_res


# def read_json(path):
#     with open(path) as json_data:
#         d = json.load(json_data)
#     return d
#
#
# def read_raster(rater_path):
#     with rasterio.open(rater_path, "r") as src:
#         arr = src.read()
#         arr = raster_to_img(arr, src.meta)
#         meta = src.meta.copy()
#     return arr, meta


def get_polygon_from_bound(bound):
    return Polygon([
        (bound[0][0], bound[0][1]),
        (bound[1][0], bound[0][1]),
        (bound[1][0], bound[1][1]),
        (bound[0][0], bound[1][1])
    ])




def delete_file(file_path):
    logger.info("Removing file {}".format(file_path))
    if os.path.isfile(file_path):
        os.remove(file_path)
        logger.info('{} was removed'.format(file_path))
    else:
        logger.info('{} does not exists'.format(file_path))




def sliding_window(raster_path, step_size, window_size):
    """
    iterate throw a raster using sliding window
    :param raster_path: path to the target raster
    :param step_size: indicates how many pixels we are going to “skip”
        in both the (h, w) direction
    :param window_size: define sliding window size in the (h, w) direction
    :return: h, w coordinates and raster's window
    """
    with rasterio.open(raster_path, 'r') as src:
        for x in range(0, src.height, step_size[0]):
            for y in range(0, src.width, step_size[1]):
                yield x, y, src.read(
                    window=Window(y, x, window_size[1], window_size[0])
                )


def resize_raster(in_raster_path, out_raster_path, scale_coef=1,
                  window_size=TILE_SIZE):
    """
    resize a raster by a longest side using sliding window approach
    :param in_raster_path: path to input raster
    :param out_raster_path: path to output(resulting) raster
    :param scale_coef: scale factor
    :param window_size: size of sliding window side
    :return:
    """
    with rasterio.open(in_raster_path, 'r') as src:
        scaled_window_size = int(window_size * scale_coef)
        scaled_h = int(src.height * scale_coef)
        scaled_w = int(src.width * scale_coef)

        options = src.profile.copy()
        options['width'] = scaled_w
        options['height'] = scaled_h
        options['transform'] = \
            src.transform * Affine.scale(1 / scale_coef, 1 / scale_coef)

    with rasterio.open(out_raster_path, 'w', **options) as dst:
        for x, y, tile in sliding_window(in_raster_path,
                                         (window_size, window_size),
                                         (window_size, window_size)):
            if len(tile.shape) == 2:
                tile = np.expand_dims(tile, axis=0)
            tile = reshape_as_image(tile)
            tile = cv2.resize(tile, (scaled_window_size, scaled_window_size))
            if len(tile.shape) == 2:
                tile = np.expand_dims(tile, axis=0)
            else:
                tile = reshape_as_raster(tile)
            scaled_y = int(y/window_size) * scaled_window_size
            scaled_x = int(x/window_size) * scaled_window_size
            dst.write(
                tile,
                window=Window(
                    scaled_y,
                    scaled_x,
                    min(scaled_window_size, scaled_w - scaled_y),
                    min(scaled_window_size, scaled_h - scaled_x)
                )
            )





def convert_points_to_utm(pts, transform):
    """
    convert points from image crs to utm crs, using not_aligned_stack transformation matrix
    :param pts: list of points to transform
    :param transform: raster meta['transform'] to convert points from image to utm
    :type transform: rasterio.affine
    """
    if not isinstance(transform, Affine):
        raise ValueError("Transform is not affine object")
    trans_pts = np.array([transform * i for i in pts])
    return trans_pts


def check_point_crs(pts, crs):
    if np.max(pts[0]) < 180 and np.min(pts[0]) > -180:
        project = partial(
            pyproj.transform,
            pyproj.Proj(init='epsg:4326'),  # source coordinate system
            pyproj.Proj(init=crs))  # destination coordinate system

        pts = [transform(project, Point(pt)) for pt in pts]
        pts = [np.array(pt) for pt in pts]
    return pts


def read_manual_pts(manual_pts, init):
    if not os.path.exists(manual_pts):
        raise FileNotFoundError("Manual points Geojson wasn't found at: {}".format(manual_pts))
    pts_df = pd.read_csv(manual_pts)
    base_pts = []
    not_aligned_pts = []
    for number, row in pts_df.iterrows():
        base_pts.append(tuple([row['mapX'], row['mapY']]))
        not_aligned_pts.append(tuple([row['pixelX'], row['pixelY']]))

    base_pts = check_point_crs(base_pts, init)
    not_aligned_pts = check_point_crs(not_aligned_pts, init)

    # Check point inside the bounding polygon
    # base_points_inside = [not_aligned_stack.grove_bounds.contains(Point(pt)) for pt in base_pts]
    # not_points_inside = [not_aligned_stack.grove_bounds.contains(Point(pt)) for pt in not_aligned_pts]
    # if np.sum(base_points_inside) / len(base_pts) < 0.5 or np.sum(not_points_inside) / len(base_pts) < 0.5:
    #     logger.error("There is only {} points inside the grove polygon,"
    #                   " but must be at least 50 %".format((np.sum(base_points_inside) * 100) / len(base_pts)))
    return base_pts, not_aligned_pts


def calculate_transform_matrix_manual(raster_path, manual_points=None, base_stack=None):
    """
    Calculates affine transform from target to base, using RANSAC algorithm
    :param base_stack: Reference stack.
    :param manual_points: manual points list
    :param not_aligned_stack: Stack that needs to be aligned.
    """

    raster = rasterio.open(raster_path,'r')
    meta = raster.meta.copy()
    raster.close()
    base_pts, not_aligned_pts = read_manual_pts(manual_points, meta['crs']['init'])
    # import ipdb; ipdb.set_trace()
    if len(base_pts) == 0:
        raise AssertionError("There is not enough points was found to align rasters")
    if len(base_pts) != len(not_aligned_pts):
        raise ValueError(
            "Length of points lists don't match len of base_pts: {}, len of base_pts: {}".format(len(base_pts),
                                                                                                 len(not_aligned_pts)))

    rev = ~meta['transform']
    base_pts_utm = convert_points_to_utm(base_pts, rev)
    not_aligned_pts_utm = convert_points_to_utm(not_aligned_pts, rev)
    transform_matrix = ProjectiveTransform()
    transform_matrix.estimate(not_aligned_pts_utm, base_pts_utm)
    not_aligned_pts_utm = np.expand_dims(not_aligned_pts_utm, axis=0)
    aligned_pts_utm = cv2.perspectiveTransform(not_aligned_pts_utm, transform_matrix.params)
    aligned_pts_utm = aligned_pts_utm[0]

    return transform_matrix, base_pts_utm, aligned_pts_utm


def transform_raster_orb_full(trans_raster, orb_transformation_matrix, save_path, transform='perspective'):
    with rasterio.open(save_path, 'w', BIGTIFF='YES', **trans_raster.meta) as dst:
        num = trans_raster.meta['count']
        logger.info(f'count is {num}')
        for i in range(num):
            band = trans_raster.read(i + 1)
            if band.dtype == 'uint8':
                if transform == 'perspective':
                    band = cv2.warpPerspective(band,
                                               orb_transformation_matrix.params,
                                               (band.shape[1], band.shape[0]),
                                               borderValue=0)
                else:
                    band = cv2.warpAffine(band,
                                          orb_transformation_matrix.params[:2],
                                          (band.shape[1], band.shape[0]),
                                          borderValue=0)
                band = band.astype(np.uint8)
                band[band < -1] = 0
            else:
                logger.info('!band.dtype == uint8')

                if transform == 'perspective':
                    band = cv2.warpPerspective(band,
                                               orb_transformation_matrix.params,
                                               (band.shape[1], band.shape[0]),
                                               borderValue=-10000)
                else:
                    band = cv2.warpAffine(band,
                                          orb_transformation_matrix.params[:2],
                                          (band.shape[1], band.shape[0]),
                                          borderValue=-10000)
                band = band.astype(np.float32)
                logger.info('band')

                band[band < -1] = -10000
            logger.info(f'dst.write(band, i + 1)')

            dst.write(band, i + 1)


def resize(input_path, output_path, scale):
    resize_raster(input_path, output_path, scale_coef=scale)
    # raster_array, meta = read_raster(input_path)
    # save_raster_path(output_path, meta, raster_array, downscale=scale)
