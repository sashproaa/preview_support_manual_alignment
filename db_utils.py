import os
import numpy as np
from shapely.geometry import Point
import geopandas as gpd

def get_index_points(point_path):
    if not os.path.exists(point_path):
        raise FileNotFoundError("Tree Index Geojson wasn't found at: {}".format(point_path))
    gdf_point = gpd.read_file(point_path)
    index_points = [np.array(pt.coords)[0] for pt in gdf_point['geometry'].tolist()]
    index_points = np.array(index_points)
    index_points = create_point_list(index_points)
    return index_points, gdf_point

def create_point_list(pts):
    """
    Creates list of Shapely Points from numpy array
    :param pts: list of numpy arrays
    :return: list of Shapely Points
    """
    if type(pts) == tuple:
        test = np.zeros((1,), dtype='f8,f8')
        test[0] = pts
        pts = test
    return [Point(i) for i in pts]
