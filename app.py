import os
import utils as utils
import seetree.gsutil as gsutil
import rasterio
import numpy as np
import cv2
from flask import Flask, request
from settings import logging, OUTPUT_FOLDER, INPUT_FOLDER

logger = logging.getLogger(__name__)
os.makedirs(OUTPUT_FOLDER, exist_ok=True)
os.makedirs(INPUT_FOLDER, exist_ok=True)

app = Flask(__name__)

@app.route('/')
def init():
    return 'preview support to manual alignment path'

# @app.route('/resize', methods = ['POST'])
# def resize_raster():
#     data = request.json
#     try:
#         raster_path = data.get('raster_path', None)
#         scale = data.get('dscale', None)
#     except AttributeError as err:
#         logger.error(f"{err}. Request header should contain content-type=application/json.")
#         return 'Request header should contain content-type=application/json.', 400
#
#     if raster_path is None:
#         return 'Raster_path is None. Check request data and header. Should be json', 400
#     if scale is None:
#         return 'Dscale is None. Check request data and header. Should be json', 400
#
#     file_name = os.path.basename(raster_path)
#     raster_output_path = os.path.join(INPUT_FOLDER, file_name)
#     try:
#
#         gsutil.cp(raster_path, raster_output_path)
#
#         resized_file_name = os.path.join(OUTPUT_FOLDER, 'resized_'+file_name)
#         scale = float(scale)
#         utils.resize(raster_output_path, resized_file_name, scale)
#     except Exception as err:
#         return str(err), 400
#
#     # logger.info('raster resized')
#     return resized_file_name
#

@app.route('/transform', methods = ['POST'])
def transform():
    logger.info('transformation request recieved')
    data = request.json

    try:
        manual_points_path = data.get('manual_points_path', None)
        raster_path = data.get('raster_path', None)
        output_path = data.get('output_path', None)
    except AttributeError as err:
        logger.error(f"{err}. Request header should contain content-type=application/json.")
        return 'Request header should contain content-type=application/json.', 400

    if 'manual_points_path' is None or 'raster_path' is None or 'output_path' is None:
        return 'There is no manual_points_path or raster_path or output_path in request data', 204

    try:
        points_path = os.path.join(INPUT_FOLDER, os.path.basename(manual_points_path))
        out_raster_path = os.path.join(INPUT_FOLDER, os.path.basename(raster_path))
        gsutil.cp(manual_points_path, points_path)
        gsutil.cp(raster_path, out_raster_path)
    except OSError as err:
        return str(err), 400

    logger.info('calculate_transform_matrix_manual started')
    transform_matrix, base_pts_utm, aligned_pts_utm =  \
        utils.calculate_transform_matrix_manual(raster_path=out_raster_path, manual_points=points_path)

    aligned_raster_path = os.path.join(OUTPUT_FOLDER, 'aligned.tif')
    aligned_jpg = os.path.join(OUTPUT_FOLDER, 'aligned.jpg')
    trans_method = 'perspective'
    trans_matrix_path = os.path.join(OUTPUT_FOLDER, 'transform_matrix.npy')
    np.save(trans_matrix_path, transform_matrix.params)
    with rasterio.open(out_raster_path, 'r') as raster_img:
        utils.transform_raster_orb_full(raster_img,
                              transform_matrix,
                              aligned_raster_path,
                              transform=trans_method)
    with rasterio.open(aligned_raster_path, 'r') as aligned_tif:
        img = utils.raster_to_img(aligned_tif.read(), aligned_tif.meta)
        cv2.imwrite(aligned_jpg, img)
    logger.info(os.path.join(OUTPUT_FOLDER, '*'))
    gsutil.cp(os.path.join(OUTPUT_FOLDER, '*'), output_path)
    return output_path, 200

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))



