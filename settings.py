import os
import logging
from logging.config import dictConfig

# this threshold used to convert nrg_bin to true false
TEMP_FOLDER = os.getenv('TEMP_FOLDER') or './'
OUTPUT_FOLDER = os.path.join(TEMP_FOLDER, 'output')
INPUT_FOLDER = os.path.join(TEMP_FOLDER, 'input')
TILE_SIZE = 4096

LOGGING_CONFIG = dict({
    'version': 1,
    'disable_existing_loggers': True,  # this fixes the problem
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s',
            'datefmt': ' %I:%M:%S '
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'

        },
    },
    'loggers': {
        'root': {
            'handlers': ['default'],
            'level': 'INFO',
        },

    },
    'root': {
        'handlers': ['default'],
        'level': 'INFO',
    },

})


logging.config.dictConfig(LOGGING_CONFIG)

DEFAULT_MODEL_VERSION_NON_ALIGNED = "v4"

WORK_ROOT = os.getenv('WORK_ROOT', '/tmp')
